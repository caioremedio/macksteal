# README #


```
#!php

 __  __            _     ____  _             _         _ 
|  \/  | __ _  ___| | __/ ___|| |_ ___  __ _| | __   _/ |
| |\/| |/ _` |/ __| |/ /\___ \| __/ _ \/ _` | | \ \ / / |
| |  | | (_| | (__|   <  ___) | ||  __/ (_| | |  \ V /| |
|_|  |_|\__,_|\___|_|\_\|____/ \__\___|\__,_|_|   \_/ |_|
```

Verifique se seus amiguinhos fizeram a tarefa no Moodle do Mackenzie!

## Requisitos ##
* PHP 5.x
* cURL (Apenas usuários de Windows precisam instalar, siga: [este guia!](http://stackoverflow.com/a/17074237/2788711))

## Como baixar? ##
Se você não manja nada de git, baixe a última versão na [página de downloads](https://bitbucket.org/caioremedio/macksteal/downloads#tag-downloads) na aba **Tabs**

## Como configurar? ##
1. Renomeie o arquivo `accounts.php.example ` para `accounts.php`
2. Insira todas as contas com suas respectivas senhas do TIA no `accounts.php`, respeitando a individualidade dos colchetes, seguindo o modelo e separando-os por vírgula.

## Como rodar? ##
### No Windows ###
Abra o `cmd` e execute o seguinte comando: 
`C:\local\do\php\php.exe -f "C:\local\do\macksteal\macksteal.php"`

### No Mac OS/Linux ###
Abra o `Terminal` e execute o seguinte comando:
`php /Local/Do/macksteal/macksteal.php`

## Como Usar? ##
Você será requisitado a digitar a ID da tarefa. Ela é obtida através da URL do Moodle, na própria página de entrega.
Por exemplo, a seguinte página de entrega possui a URL:
`http://ead.mackenzie.br/moodle/mod/assign/view.php?id=746977`
A ID da tarefa é a que está logo após `id=`, nesse caso, `746977`

## Problemas conhecidos ##
`Call to undefined function curl_init()`

Você provavelmente é usuário de Windows e não leu os requisitos. Siga esse guia para instala-lo: http://stackoverflow.com/a/17074237/2788711

## Changelog ##

#### v1 ####
```
#!php
- Versão Inicial do MackSteal
```