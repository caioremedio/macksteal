<?php

if (!@include("accounts.php")) {
	throw new Exception("Arquivo accounts.php não foi configurado!!!");
	exit;	
}

else {
	include_once "accounts.php";
}

//
//	MISC
//

$art = "__  __            _     ____  _             _         _ \n|  \/  | __ _  ___| | __/ ___|| |_ ___  __ _| | __   _/ |\n| |\/| |/ _` |/ __| |/ /\___ \| __/ _ \/ _` | | \ \ / / |\n| |  | | (_| | (__|   <  ___) | ||  __/ (_| | |  \ V /| |\n|_|  |_|\__,_|\___|_|\_\|____/ \__\___|\__,_|_|   \_/ |_|\n";

//
//	Debug
//

define("DEBUG_LOG", FALSE);

//
// URLS
//

define("URL_MOODLE_LOGIN", "http://moodle.mackenzie.br/moodle/login/index.php");
define("URL_MOODLE_HOME", "http://moodle.mackenzie.br/moodle/index.php");
define("URL_MOODLE_HOMEWORK", "http://moodle.mackenzie.br/moodle/mod/assign/view.php?id=");
define("URL_MOODLE_EXPORT", "http://moodle.mackenzie.br/moodle/calendar/export.php");

//
//	CURL SETTINGS
//

define("FILE_COOKIE_MOODLE", "./cookie_moodle.txt");
define("USER_AGENT", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0");

//
//	TEXTS
//

define("HAS_MADE_TEXT", "Enviado para avaliação");
define("HAS_NOT_MADE_TEXT", "Nenhuma tentativa");

?>