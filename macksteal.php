<?php

include_once "config.php";

//
//	NOIS É LADRAO
//

echo "\n\e[31m\e[1m $art \n\e[0m";
echo "\e[1mInicializado com \e[34m" . count($accounts) . "\e[0m \e[1mcontas!\e[0m\n\n";
echo "\e[1mDigite a ID da tarefa que irei verificar:\e[0m ";

$homework_id = PHP_OS == 'WINNT' ? stream_get_line(STDIN, 1024, PHP_EOL) : readline();

if (DEBUG_LOG) {
	echo "\n\nID = $homework_id\n\n";
}

echo "\n\n\t\e[1mOk começando!\e[0m\n\n";

$curl = initialize_curl();

foreach ($accounts as $account) {

	echo "\e[1mTIA:\e[0m " . $account["tia"] . " (" . $account["pass"] . ")" . " = ";

	$homework_status = check_user_homework_status(
							$curl, 
							$account["tia"], 
							$account["pass"], 
							$homework_id);

	$status_msg = $homework_status ? "\e[32mFEZ!\e[0m" : "\e[31mNÃO FEZ!\e[0m";

	echo "$status_msg\n";
}

echo "\n\n\e[5mNOIS EH LADRAO\e[0m\n\n";

exit;

function check_user_homework_status($curl, $tia, $pass, $homework_id) {

	//
	//	Login
	//

	curl_setopt($curl, CURLOPT_URL, URL_MOODLE_LOGIN);
	curl_setopt($curl, CURLOPT_POSTFIELDS, userMoodleLoginParamsWithTiaAndPass($tia, $pass));
	curl_exec($curl);

	//
	// Verifica o status do moodle
	//

	if (!is_moodle_online($curl)) {
		echo "\n\n\e[5m\e[1mMOODLE ESTA OFFLINE!\e[0m\n\n\tSaindo...\n\n";
		exit;
	}

	//
	//	Request na página export.php para obter o 'sesskey'
	//

	curl_setopt($curl, CURLOPT_URL, URL_MOODLE_EXPORT);
	$sessKey = get_sesskey(curl_exec($curl));

	//
	//	Request novamente na página, mas enviando o sessKey para pegar pagina de tarefa
	//
	
	curl_setopt($curl, CURLOPT_URL, (URL_MOODLE_HOMEWORK . $homework_id));
	curl_setopt($curl, CURLOPT_POSTFIELDS, ["sesskey" => $sessKey]);
	
	return has_user_made_homework(curl_exec($curl));
}

function get_sesskey($sessKeyContent) {

	$doc = new DOMDocument();
	@$doc->loadHTML($sessKeyContent);

	$sessKey;

	$inputs = $doc->getElementsByTagName("input");

	foreach($inputs as $node) {
		foreach($node->attributes as $attribute) {
		    if($attribute->nodeName == 'name' && $attribute->nodeValue == 'sesskey') {
		        $sessKey = $node->getAttribute('value');
		    }
		}
	}

	return $sessKey;	
}

function has_user_made_homework($homework_content) {

	$doc = new DOMDocument();
	@$doc->loadHTML($homework_content);

	$inputs = $doc->getElementsByTagName("div");

	foreach($inputs as $node) {
		foreach($node->attributes as $attribute) {
		    if($attribute->nodeName == 'class' && $attribute->nodeValue == 'box boxaligncenter submissionsummarytable') {
		        
		    	$trimmed_nodeValue = trim($node->nodeValue);

		    	if (!$trimmed_nodeValue || $trimmed_nodeValue == "") {
		    		continue;
		    	}

		    	if (DEBUG_LOG) {
		    		echo "\n\n" . $trimmed_nodeValue . "\n\n";
		    	}

		    	if (strpos($trimmed_nodeValue, HAS_MADE_TEXT) !== false) {
		    		return TRUE;
		    	}

		    	if (strpos($trimmed_nodeValue, HAS_NOT_MADE_TEXT) !== false) {
		    		return FALSE;
		    	}
		    }
		}
	}

	return FALSE;
}

function initialize_curl() {

	$ch = curl_init(URL_MOODLE_LOGIN);

	curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE);
	curl_setopt($ch, CURLOPT_COOKIEJAR, FILE_COOKIE_MOODLE);
	curl_setopt($ch, CURLOPT_COOKIEFILE, FILE_COOKIE_MOODLE);
	curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

	return $ch;
}

function is_moodle_online($curl) {
	return curl_getinfo($curl, CURLINFO_HTTP_CODE) == 200 ||
		   curl_getinfo($curl, CURLINFO_HTTP_CODE) == 303;
}

/**
 * Monta a URL de método POST do Moodle do Mackenzie.
 * @param  Tia do usuário
 * @param  Senha do usuário
 * @return Dicionário montado
 */
function userMoodleLoginParamsWithTiaAndPass ($userTia, $userPass) {

	return ["username" => $userTia,
			"password" => $userPass];
}

?>
